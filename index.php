<?php include 'head.php';?>

{{awesome-styles}}
<html>
<head>
    <style>
    /*=============== ADD YOUR CUSTOM STYLES HERE ==============*?
    </style>
</head>
<body>

    <section class="super-body">

        <!-- Fixed navbar -->
        <nav class="navbar navbar-inverse">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    <a class="navbar-brand" href="#"><i class="fa fa-play-circle" aria-hidden="true"></i></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="#">Home</a></li>
                        <li><a href="#about">About</a></li>
                        <li><a href="#contact">Contact</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li class="dropdown-header">Nav header</li>
                                <li><a href="#">Separated link</a></li>
                                <li><a href="#">One more separated link</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="http://www.gcpedia.gc.ca/gcwiki/index.php?title=Loogart_Labs&action=edit">Edit</a></li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </nav>

        <!-- Main component for a primary marketing message or call to action -->
        <div class="jumbotron red">
            <div class="container">
                <h5 class="box">Special Edition</h5>
                <h2>GCPedia</h2>
                <h1>Awesome (template)</h1>
                <div class="row">
                    <div class="col-sm-offset-3 col-sm-6">
                        <p style="letter-spacing:1px;">Some tag line that will inspire people. Or some quote from Instagram.</p>
                    </div>
                </div>
            </div>
            <!-- /container -->
        </div>

        <!-- content -->
        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                    <h2><b>How to edit</b></h2>
                    <p>Simple way to make GCPedia a little more interesting. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero, commodi! Nulla animi iure, nisi enim reiciendis veniam nobis consequatur non, sint, magni culpa repudiandae distinctio facere numquam. Tempore, facilis, nulla!</p>
                </div>
                <div class="col-sm-7">
                    <h2><b>How to edit</b></h2>
                    <p>Simple way to make GCPedia a little more interesting. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero, commodi! Nulla animi iure, nisi enim reiciendis veniam nobis consequatur non, sint, magni culpa repudiandae distinctio facere numquam. Tempore, facilis, nulla!</p>
                </div>

            </div>
        </div>


    </section>


</body>

</html>

